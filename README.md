# Maplecroft Twitter feed demo

## Installation

First clone the git repository

```
git clone https://bitbucket.org/peterseymour/maplecroft
```

Install virtualenv if not installed on your OS. For Ubuntu run

```
sudo apt-get install python-virtualenv
```

Configure the virtualenv

```
cd maplecroft
virtualenv venv --python=python3
echo "export \$(cat .env)" >> venv/bin/activate
touch .env
```

Edit the *.env* and add the following enironment variables

```
TWITTER_CONSUMER_KEY=
TWITTER_CONSUMER_SECRET=
TWITTER_ACCESS_TOKEN=
TWITTER_ACCESS_TOKEN_SECRET=
GOOGLE_MAPS_API_KEY=
```

Then enter the environment and confiure

```
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Create the database and import data

```
./manage.py migrate
./manage.py import_countries data/countries.csv
```

Finally run the server and visit [http://127.0.0.1:8000]

```
./manage.py runserver
```

## About the app

From the front page link to the twitter feed page.
This shows the latest 10 tweets and then grabs any tag on each tweet to do a country lookup.
The matching tags are shown as markers at the coordinates specified in the *countries.csv* file.
Hovering over the marker shows the country name and the id of the tweets that mention that country, you may have to zoom in or out where markers overlap.
Showing more that 10 tweets populates the map with more information.
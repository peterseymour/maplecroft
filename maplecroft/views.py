from django.shortcuts import render

def index( request ):
	return render( request, 'maplecroft/index.html' )

from django.conf.urls import url, include
from . import views
#from django.contrib import admin

urlpatterns = [
	#url( r'^admin/', admin.site.urls ),

	url( r'^$', views.index ),
	url( r'^twitterfeed/', include( 'twitterfeed.urls', namespace='twitterfeed' ) ),
]

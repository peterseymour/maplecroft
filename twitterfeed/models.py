from django.db import models

class Country(models.Model):
	name = models.CharField( max_length=128, db_index=True )
	code = models.CharField( max_length=2 )
	longitude = models.DecimalField( max_digits=8, decimal_places=5 )
	latitude = models.DecimalField( max_digits=8, decimal_places=5 )

	def __str__( self ):
		return self.name

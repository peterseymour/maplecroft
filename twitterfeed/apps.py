from django.apps import AppConfig


class TwitterfeedConfig(AppConfig):
    name = 'twitterfeed'

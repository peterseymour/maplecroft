from django.core.management.base import BaseCommand, CommandError
from twitterfeed.models import Country

import csv
from decimal import Decimal

class Command(BaseCommand):
	help = 'Imports countries from CSV file'

	def add_arguments( self, parser ):
		parser.add_argument( 'filename', nargs=1, type=str )

	def handle( self, *args, **options ):
		filename = options['filename'][0]
			
		ncountries = 0
		nrows = 0
		with open( filename, 'r' ) as csvfile:
			reader = csv.reader( csvfile )
			
			#skip header
			for row in reader:
				break

			for row in reader:
				if len(row) == 0:
					continue

				name, code, longitude, latitude = row

				if longitude != "None" and latitude != "None":
					Country( name=name, code=code, latitude=Decimal( latitude ), longitude=Decimal( longitude ) ).save()

					ncountries += 1

				nrows += 1

		self.stdout.write( self.style.SUCCESS( 'Successfully imported %d/%d countries' % (ncountries, nrows) ) )
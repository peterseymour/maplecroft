from django.shortcuts import render
from django.conf import settings
from .models import Country
import twitter
import re

RX = re.compile( "#([a-zA-Z0-9]+)" )

def feedmap( request ):

	api = twitter.Api(
			consumer_key = settings.TWITTER_CONSUMER_KEY,
			consumer_secret = settings.TWITTER_CONSUMER_SECRET,
			access_token_key = settings.TWITTER_ACCESS_TOKEN,
			access_token_secret = settings.TWITTER_ACCESS_TOKEN_SECRET )

	tweets = api.GetUserTimeline( screen_name="MaplecroftRisk", count=10 )

	for i, tweet in enumerate( tweets ):
		tweet.id = i + 1

	tweets_by_country = {}

	for tweet in tweets:
		for tag in RX.findall( tweet.text ):
			countries = Country.objects.filter( name=tag )
			if countries.count() > 0:
				country = countries.first()

				tweets_by_country.setdefault( country, [] ).append( tweet.id )

	midpoint = (
		sum( map( lambda country: country.latitude, tweets_by_country.keys() ) ) / len(tweets_by_country),
		sum( map( lambda country: country.longitude, tweets_by_country.keys() ) ) / len(tweets_by_country) )

	return render( request, 'twitterfeed/feedmap.html', {
			'tweets': tweets,
			'tweets_by_country': tweets_by_country,
			'midpoint': midpoint,
			'GOOGLE_MAPS_API_KEY': settings.GOOGLE_MAPS_API_KEY
		} )
